#!/usr/bin/env python3
import argparse
import asyncio
import logging
import re
import socket
import uuid
from enum import Enum
from typing import Awaitable

log = logging.getLogger(__name__)
log.addHandler(logging.StreamHandler())
log.setLevel(logging.INFO)

HOSTNAME = socket.getfqdn()
APPLICATION_NAME = '1337Mail'


class MessageType(Enum):
    HELO = 'HELO'
    EHLO = 'EHLO'
    MAIL_FROM = 'MAIL FROM'
    RCPT_TO = 'RCPT TO'
    DATA = 'DATA'
    SUBJECT = 'Subject'
    END = '.'
    QUIT = 'QUIT'


class BreakRulesException(Exception):
    ...


class NotEnoughArgumentsException(Exception):
    ...


def is_mail_address(address):
    pattern = r'[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+'

    return re.fullmatch(pattern, address)


def parse_mail_address(message):
    address = ''.join([char for char in message if char not in {'<', '>', ' '}])

    return address


async def helo(addr, reader, writer):
    data = await reader.readline()
    message = data.decode().split()

    log.info(f'{addr} <-- {message}')

    if not len(message) > 1:
        raise NotEnoughArgumentsException

    if message[0] == MessageType.EHLO.value:
        log.info(f'{addr} --> 502 Error: command not recognized')

        writer.write(bytes('502 5.5.2 Error: command not recognized\n', 'utf-8'))
        await writer.drain()

        data = await reader.readline()
        message = data.decode().split()
        log.info(f'{addr} <-- {message}')

    if not message[0] == MessageType.HELO.value:
        raise BreakRulesException

    log.info(f'{addr} --> 250 {HOSTNAME}')
    writer.write(bytes('250 {HOSTNAME}\n', 'utf-8'))
    await writer.drain()

    return message[1]


async def mail_from(addr, reader, writer):
    data = await reader.readline()
    message = data.decode().strip().split(':')

    log.info(f'{addr} <-- {message}')

    if not len(message) > 1:
        raise NotEnoughArgumentsException

    if not message[0] == MessageType.MAIL_FROM.value:
        raise BreakRulesException

    sender = parse_mail_address(message[1])
    if not is_mail_address(sender):
        raise BreakRulesException

    log.info(f'{addr} --> 250 2.1.0 Ok')
    writer.write(bytes(f'250 2.1.0 Ok\n', 'utf-8'))
    await writer.drain()

    return message[1]


async def rcpt_to(addr, reader, writer):
    data = await reader.readline()
    message = data.decode().strip().split(':')

    log.info(f'{addr} <-- {message}')

    if not len(message) > 1:
        raise NotEnoughArgumentsException

    if not message[0] == MessageType.RCPT_TO.value:
        raise BreakRulesException

    recipient = parse_mail_address(message[1])
    if not is_mail_address(recipient):
        raise BreakRulesException

    log.info(f'{addr} --> 250 Ok')
    writer.write(bytes(f'250 {recipient}... Recipient OK\n', 'utf-8'))
    await writer.drain()

    return message[1]


async def data(addr, reader, writer):
    # get data command
    data = await reader.readline()
    message = data.decode().strip().split()

    log.info(f'{addr} <-- {message}')

    if not len(message) > 0:
        raise NotEnoughArgumentsException

    if not message[0] == MessageType.DATA.value:
        raise BreakRulesException

    log.info(f'{addr} --> 354 Please start mail input, and finish with a new line and a \'.\' followed by a newline')
    writer.write(bytes('354 Please start mail input, and finish with a new line and a \'.\' followed by a newline\n', 'utf-8'))
    await writer.drain()

    # get body
    body = []
    initiate_end = False

    while True:
        data = await reader.readline()
        line = data.decode().strip()
        log.info(f'{addr} <-- {line}')

        if initiate_end:
            if data == b'\n':
                break

            initiate_end = False

        if line.strip() == MessageType.END.value:
            break
            initiate_end = True
            continue

        body.append(data.decode())
        await asyncio.sleep(0.01)

    log.info(f'{addr} --> 250 Mail queued for delivery.')
    writer.write(bytes('250 Mail queued for delivery.\n', 'utf-8'))
    await writer.drain()

    return body


async def close_channel(addr, reader, writer):
    data = await reader.readline()
    message = data.decode().strip().split()

    log.info(f'{addr} <-- {message}')

    if not len(message) > 0:
        raise NotEnoughArgumentsException

    if not message[0] == MessageType.QUIT.value:
        raise BreakRulesException

    log.info(f'{addr} --> 221 {HOSTNAME} Service closing transmission channel')
    writer.write(bytes(f'221 {HOSTNAME} Service closing transmission channel', 'utf-8'))

    writer.close()


async def write_body_to_file(body):
    filename = str(uuid.uuid4())
    path = '/'.join(['/tmp/mail', filename])

    log.info(f'[*] Writing mail to: {path}')
    with open(path, 'w') as f:
        f.write(''.join(body))

    return


async def handle_connection(reader, writer):
    writer.write(bytes(f'220 {HOSTNAME} SMTP {APPLICATION_NAME}\n', 'utf-8'))
    await writer.drain()

    addr = writer.get_extra_info('peername')
    log.info(f'[*] Connection from: {addr}')

    try:
        domain = await helo(addr, reader, writer)
        sender = await mail_from(addr, reader, writer)
        recipient = await rcpt_to(addr, reader, writer)
        body = await data(addr, reader, writer)
        await close_channel(addr, reader, writer)
    except (BreakRulesException, NotEnoughArgumentsException):
        log.info(f'{addr} --> I can break rules too, bye.')
        writer.write(bytes('I can break rules too, bye.', 'utf-8'))
        await writer.drain()
        writer.close()
        return

    try:
        await write_body_to_file(body)
    except OSError as err:
        log.error(err)

    try:
        writer.close()
    except:
        pass

    return


async def main(args) -> Awaitable:
    server = await asyncio.start_server(
        handle_connection, args.host, args.port
    )

    addr = server.sockets[0].getsockname()
    log.info(f'Listening on {addr}')

    async with server:
        await server.serve_forever()


def parse_args():
    args = argparse.ArgumentParser()
    args.add_argument('--host', help='Address to listen on', default='0.0.0.0')
    args.add_argument('--port', help='Port to listen on', default=25)

    return args.parse_args()


if __name__ == '__main__':
    args = parse_args()

    asyncio.run(main(args))
