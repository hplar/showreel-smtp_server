# Showreel STMP Server

This simple SMTP server implementation only accepts incoming connections and follows
the SMTP protocol to receive incoming e-mail and write the mail to disk.
It is not meant to be run on any kind of production environment.

## Requirements

* Python 3.9
