# vim: ts=2 sw=2

.PHONY: tags

clean:
		@echo -n "Cleaning repository... "
		@find src -type d -name "__pycache__" -exec rm -rf {} \;
		@find src -type d -name ".mypy_cache" -exec rm -rf {} \;
		@find src -type f -name "*.pyc" -exec rm -rf {} \;
		@rm -rf .mypy_cache
		@rm -rf .pytest_cache
		@rm -rf .coverage
		@echo "done"

tags:
	@echo -n "Cleaning tags... "
	@rm -rf ./tags
	@echo "done"
	@echo -n "Generating new tags... "
	@ctags --languages=python -R smtp_server/ tests/
	@echo "done"

serve:
	PYTHONPATH=. ./smtp_server/server.py

venv:
	python3 -m venv venv
	./venv/bin/python3 -m pip install -r req/base.txt


### DEVELOPMENT
venv/dev:
	python3 -m venv venv
	./venv/bin/python3 -m pip install -r req/base.txt -r req/dev.txt  -r req/test.txt -r req/tox.txt


## TESTS
test:
	tox -e mypy,lint,isort,py38

test/lint:
	tox -e lint,isort,mypy

test/unit:
	tox -e py38
